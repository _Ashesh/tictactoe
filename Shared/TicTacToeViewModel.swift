//
//  TicTacToeViewModel.swift
//  TicTacToe (iOS)
//
//  Created by Ashesh Patel on 2021-06-23.
//

import SwiftUI

final class TicTacToeViewModel: ObservableObject{
    
    let columns : [GridItem]
        = [
            GridItem(.flexible()),
            GridItem(.flexible()),
            GridItem(.flexible()),
            
        ]
    
    
    @Published var moves : [move?] = Array(repeating: nil, count: 9 )
    
    @Published var  isGameOn = false
    
    @Published var alertItem: AlertItem?
    
    @Published var hintIndex = 0
    
    @Published  var hintPlayerIndication = false
    
    
    
    
    func process(in position: Int){
        
        
        //MARK:- player move process
        
        if isPlaceOccupied(in: moves, forIndex: position){return}
        
        moves[position] = move(player:  .human  , boardIndex: position)
        
        
        
        
        
        if checkWinCondition(in: moves, for: .human) {
            
            print("win win win ")
            
            alertItem = AlertContext.humanWin
            
            hintPlayerIndication = false
            
            hintIndex = 0
            
            return
        }
        
        
        
        
        if checkDrawCondition(in: moves){
            
            alertItem = AlertContext.draw
            print("drawwwww")
            
            hintPlayerIndication = false
            
            hintIndex = 0
            
            return
        }
        
        isGameOn = true
        
        
        
        //MARK:- computer move process || in future it can be online user move
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            
            let computerPosition  = determineComputerMovePosition(in: moves)
            
            
            
            moves[computerPosition] = move(player: .computer, boardIndex: computerPosition)
            
            
            isGameOn = false
            
            if checkWinCondition(in: moves, for: .computer) {
                
                alertItem = AlertContext.computerWin
                
                hintPlayerIndication = false
                
                hintIndex = 0
                
                print("win win win copa")
                return
            }
            
            if checkDrawCondition(in: moves){
                
                alertItem = AlertContext.draw
                
                hintPlayerIndication = false
                
                hintIndex = 0
                
                print("drawwwww")
                return
            }
            
            
        }
    }
    
    
    //MARK:- reset game
    func resetGame(){
        
        moves = Array(repeating: nil, count: 9)
        
    }
    
    
    
    
    //MARK:- hint for next best move for player
    func hintPlayer(in moves :[move?]) -> Int {
        
        
        
        
        
        let winPatterns : Set <Set <Int>> = [[0,1,2],
                                             [3,4,5],
                                             [6,7,8],
                                             [0,3,6],
                                             [1,4,7],
                                             [2,5,8],
                                             [0,4,8],
                                             [2,4,6]
        ]
        
        let playerMoves = moves.compactMap{$0}.filter{ $0.player == .human}
        
        let playerPositions = Set(playerMoves.map{ $0.boardIndex})
        
        for moveCombination in winPatterns{
            let winCombination = moveCombination.subtracting(playerPositions)
            
            if winCombination.count == 1{
                let isAvailable = !isPlaceOccupied(in: moves, forIndex: winCombination.first!)
                
                if isAvailable {
                    
                    print(winCombination.first!)
                    
                    hintPlayerIndication.toggle()
                    
                    if !isPlaceOccupied(in: moves, forIndex: winCombination.first!) {
                        return winCombination.first!
                        
                    }
                    
                }
            }
            
        }
        
        let computerMoves = moves.compactMap{$0}.filter{ $0.player == .computer}
        
        let computerPositions = Set(computerMoves.map{ $0.boardIndex})
        
        for moveCombination in winPatterns{
            let winCombination = moveCombination.subtracting(computerPositions)
            
            if winCombination.count == 1{
                let isAvailable = !isPlaceOccupied(in: moves, forIndex: winCombination.first!)
                
                if isAvailable {
                    
                    print(winCombination.first!)
                    
                    hintPlayerIndication.toggle()
                    
                    if !isPlaceOccupied(in: moves, forIndex: winCombination.first!) {
                        
                        return winCombination.first!
                        
                    }
                    
                    
                }
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
        if !isPlaceOccupied(in: moves, forIndex: 4) {
            
            hintPlayerIndication.toggle()
            
            return 4
        }
        
        return 1
    }
    
    
    //MARK:- Win game state tester
    
    func checkWinCondition(in moves1 :[move?]  , for player : Player ) -> Bool{
        
        let winPatterns : Set <Set <Int>> = [[0,1,2],
                                             [3,4,5],
                                             [6,7,8],
                                             [0,3,6],
                                             [1,4,7],
                                             [2,5,8],
                                             [0,4,8],
                                             [2,4,6]
        ]
        
        let playerMoves = moves1.compactMap { $0 }.filter{ $0.player == player }
        
        let playerPositions = Set(playerMoves.map{ $0.boardIndex})
        
        for pattern in winPatterns where
            pattern.isSubset(of : playerPositions ){
            
            return true
        }
        
        return false
        
        
    }
    
    
    //MARK:- Draw game state tester
    func checkDrawCondition (in moves :[move?] ) -> Bool {
        
        return moves.compactMap{$0}.count == 9
        
        
        
    }
    
    
    //MARK:-  place is occupied or not  tester
    func isPlaceOccupied(in moves: [move?] , forIndex index: Int) -> Bool{
        return moves.contains(where: { $0?.boardIndex == index  })
    }
    
    
    
    //MARK:- next move by A.I. || in future it can be online user's move process
    func determineComputerMovePosition(in moves : [move?]) -> Int {
        
        
        
        //MARK:- Win game state for A.I
        let winPatterns : Set <Set <Int>> = [[0,1,2],
                                             [3,4,5],
                                             [6,7,8],
                                             [0,3,6],
                                             [1,4,7],
                                             [2,5,8],
                                             [0,4,8],
                                             [2,4,6]
        ]
        
        
        
        
        
        let computerMoves = moves.compactMap{$0}.filter{ $0.player == .computer}
        
        let computerPositions = Set(computerMoves.map{ $0.boardIndex})
        
        for moveCombination in winPatterns{
            let winCombination = moveCombination.subtracting(computerPositions)
            
            if winCombination.count == 1{
                let isAvailable = !isPlaceOccupied(in: moves, forIndex: winCombination.first!)
                
                if isAvailable {
                    
                    print(winCombination.first!)
                    
                    
                    
                    return winCombination.first!
                    
                    
                    
                }
            }
            
            
            
        }
        
        //MARK:- Block game stratergy for A.I.
        
        let playerMoves = moves.compactMap{$0}.filter{ $0.player == .human}
        
        let playerPositions = Set(playerMoves.map{ $0.boardIndex})
        
        for moveCombination in winPatterns{
            let winCombination = moveCombination.subtracting(playerPositions)
            
            if winCombination.count == 1{
                let isAvailable = !isPlaceOccupied(in: moves, forIndex: winCombination.first!)
                
                if isAvailable {
                    
                    print(winCombination.first!)
                    
                    
                    
                    return winCombination.first!
                    
                    
                    
                }
            }
            
        }
        
        
        //MARK:- center box of the game board is 4
        if !isPlaceOccupied(in: moves, forIndex: 4) {
            
            return 4
        }
        
        
        var movePosition = Int.random(in: 0..<9)
        
        while isPlaceOccupied(in: moves, forIndex: movePosition){
            movePosition = Int.random(in: 0..<9)
        }
        
        
        
        
        
        return movePosition
    }
    
    

    
    
}
