//
//  ContentView.swift
//  Shared
//
//  Created by Ashesh Patel on 2021-06-23.
//

import SwiftUI

struct TicTacToeGame: View {
    
    @StateObject private var viewModel = TicTacToeViewModel()
    
    
    var body: some View {
        
        GeometryReader {
            g in
            
            VStack{
                Text("Tic Tac Toe")
                    .font(.headline)
                    .fontWeight(.bold)
                    .multilineTextAlignment(.center)
                
                Spacer()
                LazyVGrid(columns: viewModel.columns,spacing : 5, content: {
                    ForEach(0..<9){ i in
                        
                        ZStack{
                            
                            Circle()
                                .foregroundColor(Color(red: 0.263, green: 0.384, blue: 0.65))
                                .opacity(0.5)
                                .frame(width: g.size.width / 3 - 15,
                                       height: g.size.width / 3 - 15)
                                .blur(radius: 4)
                                .offset(x: 2, y: 2)
                                .mask(Circle().fill(LinearGradient(Color.black, Color.clear)))
                                
                                .overlay(
                                    ZStack{
                                        if viewModel.hintPlayerIndication && viewModel.hintIndex == i && !viewModel.isPlaceOccupied(in: viewModel.moves, forIndex: i){
                                            Circle()
                                                .stroke(Color.green, lineWidth: 2.0)
                                                .shadow(radius: 5)
                                        }
                                    }
                                )
                            
                            Image(systemName: viewModel.moves[i]?.ticIndicator ?? "")
                                .resizable()
                                .frame(width: 40, height: 40)
                                .foregroundColor(.primary)
                            
                        }
                        
                        .onTapGesture {
                            
                            
                            viewModel.process(in : i)
                            
                        }
                        
                        
                    }
                })
                
                Spacer()
                
                HStack{
                    
                    Button(action: {
                        DispatchQueue.main.async {
                            
                            
                            
                            viewModel.resetGame()
                            viewModel.hintIndex = 0
                            if  viewModel.hintPlayerIndication {
                          viewModel.hintPlayerIndication =
                                !viewModel.hintPlayerIndication
                            }
                            
                        }
                    }){
                        Text("Restart")
                        
                    }
                    .padding()
                    .background(Color(red: 0, green: 0, blue: 0.5))
                    .foregroundColor(.white)
                    .clipShape(Capsule())
                    .shadow(color: .gray, radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 5, y: 5)
                        
                    
                
                Button(action: {
                    DispatchQueue.main.async {
                        
                        
                        
                        viewModel.hintIndex = viewModel.hintPlayer(in: viewModel.moves)
                    }
                }){
                    Text("Hint")
                    
                }
                .padding()
                .background(Color(red: 0, green: 0, blue: 0.5))
                .foregroundColor(.white)
                .clipShape(Capsule())
                .shadow(color: .gray, radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 5, y: 5)
                    
                
                
                }
                
                
            }
            .disabled(viewModel.isGameOn)
            .padding()
            .alert(item: $viewModel.alertItem, content: {
                
                alertItem in
                
                Alert(title: alertItem.title,
                      message: alertItem.message,
                      dismissButton: .default(alertItem.buttonTitle, action: {viewModel.resetGame()}))
            })
            
            
        }
        
        
    }
    
    
}





struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TicTacToeGame()
    }
}
extension LinearGradient {
    init(_ colors: Color...) {
        self.init(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
}
