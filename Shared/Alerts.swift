//
//  Alerts.swift
//  TicTacToe (iOS)
//
//  Created by Ashesh Patel on 2021-06-23.
//

import SwiftUI



struct  AlertItem : Identifiable{
    let id = UUID()
    var title : Text
    var message : Text
    var buttonTitle : Text
    
}

struct AlertContext{
    
    
  static  let humanWin  = AlertItem(title: Text("player win"),
                              message: Text("beat the bot"),
                              buttonTitle: Text("oh yeah!")
                            )
    
    
  static  let computerWin  = AlertItem(title: Text("player lost!"),
                              message: Text("beaten by the bot"),
                              buttonTitle: Text("try again!")
                            )
    
    
   static let draw  = AlertItem(title: Text("draw"),
                              message: Text("gg"),
                              buttonTitle: Text("dang!")
                            )
    
}
