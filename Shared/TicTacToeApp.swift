//
//  TicTacToeApp.swift
//  Shared
//
//  Created by Ashesh Patel on 2021-06-23.
//

import SwiftUI

@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            TicTacToeGame()
        }
    }
}
