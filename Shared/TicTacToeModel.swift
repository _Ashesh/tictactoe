//
//  TicTacToeModel.swift
//  TicTacToe (iOS)
//
//  Created by Ashesh Patel on 2021-06-24.
//

import Foundation


enum Player{
    
    case human, computer
}

struct  move {
    let player : Player
    let boardIndex : Int
    
    
    var ticIndicator : String {
        return player == .human ? "xmark" : "circle"
    }
}
